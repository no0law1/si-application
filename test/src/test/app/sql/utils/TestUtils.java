package test.app.sql.utils;


import java.sql.Timestamp;

import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import app.sql.utils.SQLUtils;

public class TestUtils {

	@Test
	public void testStringToDate(){
		String date = "2014-12-12 12:12:12";
		
		@SuppressWarnings("deprecation")
		Timestamp date1 = new Timestamp(2014-1900, 12-1, 12, 12, 12, 12, 0);
		System.out.println(SQLUtils.stringToDate(date));
		
		assertThat(SQLUtils.stringToDate(date), is(equalTo(date1)));
	}
}
