package app.sql.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import app.exceptions.InvalidDateException;


public class SQLUtils {
	
	public static void printResultSet(ResultSet dr)
	{
		try{
			ResultSetMetaData md = dr.getMetaData();
			int columns = md.getColumnCount();
			while(dr.next()){
				for (int i = 1; i <= columns; i++) {
					String id = md.getColumnName(i);
					System.out.println(id+": "+dr.getObject(id));
				}
				System.out.println();
			}
		}catch (SQLException e) {
			System.out.println("Error");
		}
	}

	/**
	 * Given a String with a date in the following format year-month-day hour:min:sec
	 * returns a Date containing the information in the String.
	 * @param date, String containing the information of the date.
	 * @return Date described in the string.
	 */
	public static Timestamp stringToDate(String date)
	{
		if(date == null)
			throw new IllegalArgumentException("Null date");
		
		int year, month, day, hour, minute, second;
		Long msec;
		String [] split, days, hours;
		
		split = date.split(" ");
		days = split[0].split("-");
		hours = split[1].split(":");
		
		if(split.length != 2 || days.length != 3 || hours.length != 3)
			throw new IllegalArgumentException("Invalid date");
		
		try 
		{
			year = Integer.parseInt(days[0]);
			month = Integer.parseInt(days[1]);
			day = Integer.parseInt(days[2]);
			hour = Integer.parseInt(hours[0]);
			minute = Integer.parseInt(hours[1]);
			second = Integer.parseInt(hours[2]);
			
			Calendar calendar = Calendar.getInstance();
			calendar.clear();
			calendar.set(year, month-1, day, hour, minute, second);
			msec = calendar.getTimeInMillis();
			
			return new Timestamp(msec);
		} 
			catch (NumberFormatException e) 
		{
			throw new InvalidDateException("Invalid date");
		}
	}
	
	/**
	 * Sets the data in a given string in a PreparedStatement.
	 * If the given string is empty a null will be set in the PreparedStatement.
	 * @param pstmt, PreparedStatement in which the data will be inserted.
	 * @param sqlType, type of the data in the sql. This parameter is only used if a null is set.
	 * @param type, Type of the data.
	 * @param position, Position of the data in the PreparedStatement.
	 * @param data, data to be set in the PreparedStatement.
	 * @throws SQLException 
	 */
	public static void setInPreparedStatement(PreparedStatement pstmt, int sqlType,TypeOfStatementAceptedData type, int position, String data)
	throws SQLException{
		if(data == null || pstmt == null || type == null || data == null)
			throw new IllegalArgumentException("null argument");
		
		if(data.equals("null"))
		{
			pstmt.setNull(position, sqlType);
			return;
		}
		
		switch(type)
		{
			case Int : pstmt.setInt(position, Integer.parseInt(data));
			break;
			
			case String : pstmt.setString(position, data);
			break;
			
			case Data: pstmt.setTimestamp(position, stringToDate(data));
			break;
			
			case Float: pstmt.setFloat(position, Float.parseFloat(data));
			break;
			
		}
	}
}
