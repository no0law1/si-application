package app.sql;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Connection {
	
	private static String connectionString = "jdbc:sqlserver://localhost\\MSSQLSERVER;integratedSecurity=true;databaseName=PROJECT";
	
	private java.sql.Connection connection;
	
	public void closeConnection() throws SQLException {
		if(connection!=null)
			connection.close();
	}
	
	public java.sql.Connection getConnection() throws SQLException {
		return connection = DriverManager.getConnection(connectionString);
	}
	
}
