package app.querys;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Class which purpose is to load a query that is 
 * stored in a file
 * @author Jo�o Forja
 *
 */
public class QueryHandler {
	
	private BufferedReader in;
	
	private HashMap<String, String> querys;
	
	/**
	 * Instances a QueryHandler with a given file.
	 * @param path, path to the file
	 */
	public QueryHandler() {
		querys = new HashMap<>();
	}
	
	/**
	 * 
	 * @return A string with the query which was stored in
	 * the given file.
	 * @throws FileNotFoundException 
	 */
	public String getQuery(String key) {
		if(!querys.containsKey(key)){
			try {
				in = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(key)));

				StringBuffer strb = new StringBuffer();
				String line = null;
				while((line = in.readLine()) != null){
					strb.append(line+"\n");
				}
				in.close();
				
				querys.put(key, strb.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return querys.get(key);
	}
	
}
