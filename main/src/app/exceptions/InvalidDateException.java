package app.exceptions;

/**
 * Exception which is meant to be thrown when a date in introduced with
 * a wrong format.
 * @author Jo�o Forja
 *
 */
public class InvalidDateException extends RuntimeException {

	/**
	 * Id of the exception
	 */
	private static final long serialVersionUID = -3598769515306746834L;
	
	public InvalidDateException (String message)
	{
		super(message);
	}
}
