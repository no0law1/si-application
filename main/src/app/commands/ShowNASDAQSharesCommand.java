package app.commands;

import java.sql.ResultSet;
import java.sql.SQLException;

import app.commands.factory.Command;
import app.commands.factory.CommandInterface;
import app.sql.utils.SQLUtils;

public class ShowNASDAQSharesCommand extends Command implements CommandInterface{
	
	private static final String PATH = "/app/commands/querys/ShowClientsNASDAQSharesQuery.txt";

	public ShowNASDAQSharesCommand() {
		super();
	}

	@Override
	public void execute() {
		ResultSet rs=null;
		
		try {
			rs = connection.getConnection().prepareStatement(handler.getQuery(PATH)).executeQuery();
			SQLUtils.printResultSet(rs);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				if(rs!=null)
					rs.close();
				this.closeAll();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
