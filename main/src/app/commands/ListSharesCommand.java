package app.commands;

import java.sql.SQLException;
import java.sql.Timestamp;

import app.commands.factory.Command;
import app.commands.factory.CommandInterface;
import app.sql.utils.SQLUtils;

public class ListSharesCommand extends Command implements CommandInterface{
	
	private static final String PATH = "/app/commands/querys/ListSharesQuery.txt";

	public ListSharesCommand() {
		super();
	}
	
	@Override
	public void execute() {
		Timestamp dateB, dateE;
		
		try {
			System.out.println("List all executed Shares in determined Interval");
			System.out.println(">>>>>>>>>>  YYYY-MM-DD HH:MM:SS <<<<<<<<<<<");
			System.out.print("Begin Date:");
			dateB = SQLUtils.stringToDate(s.nextLine());
			System.out.print("End Date:");
			dateE = SQLUtils.stringToDate(s.nextLine());
			
			pstmt = connection.getConnection().prepareStatement(handler.getQuery(PATH));
			pstmt.setTimestamp(1, dateB);
			pstmt.setTimestamp(2, dateE);
			SQLUtils.printResultSet(pstmt.executeQuery());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				this.closeAll();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
}
