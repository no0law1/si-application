package app.commands;

import java.sql.SQLException;
import java.sql.Types;

import app.commands.factory.Command;
import app.commands.factory.CommandInterface;
import app.sql.utils.SQLUtils;
import app.sql.utils.TypeOfStatementAceptedData;


public class CreateWalletCommand extends Command implements CommandInterface{
	
	private static final String PATH = "/app/commands/querys/CreateWalletQuery.txt";
	
	public CreateWalletCommand() {
		super();
	}
	
	@Override
	public void execute() {
		Integer clientID, code, oNumber;
		Float curr, closing;
		String designation;
		
		try {
			System.out.print("Client ID: ");
			clientID = s.nextInt(); s.nextLine();
			System.out.print("Wallet Code: ");
			code = s.nextInt();
			System.out.print("Order Number: ");
			oNumber = s.nextInt();
			System.out.print("Current Value: ");
			curr = s.nextFloat();
			System.out.print("Closing Value: ");
			closing = s.nextFloat(); s.nextLine();
			System.out.print("Designation: ");
			designation = s.nextLine();
			
			pstmt = connection.getConnection().prepareStatement(handler.getQuery(PATH));
			pstmt.setInt(1, code);
			pstmt.setInt(2, oNumber);
			pstmt.setFloat(3, curr);
			pstmt.setFloat(4, closing);
			SQLUtils.setInPreparedStatement(pstmt, Types.VARCHAR, TypeOfStatementAceptedData.String, 5, designation);
			pstmt.setInt(6, clientID);
			
			pstmt.execute();			
			System.out.printf("Wallet %d successfully introduced\n", code);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				this.closeAll();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
