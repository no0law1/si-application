package app.commands;

import java.sql.Connection;
import java.sql.SQLException;

import app.commands.factory.Command;
import app.commands.factory.CommandInterface;

public class RemoveClientCommand extends Command implements CommandInterface{
	
	private static final String DELETE_ORDER = "/app/commands/querys/DeleteOrder.txt";
	
	private static final String DELETE_MANAGE_MANAGER_WALLET = "/app/commands/querys/DeleteManageManagerWallet.txt";

	private static final String DELETE_RESP_MANAGER_WALLET = "/app/commands/querys/DeleteRespManagerWallet.txt";

	private static final String DELETE_SHARE_WALLET_REGISTER = "/app/commands/querys/DeleteShareWalletRegister.txt";

	private static final String DELETE_SHARE_WALLET = "/app/commands/querys/DeleteShareWallet.txt";
	
	private static final String DELETE_WALLET_REGISTER = "/app/commands/querys/DeleteWalletRegister.txt";

	private static final String DELETE_WALLET = "/app/commands/querys/DeleteWallet.txt";

	private static final String SET_REF_CLIENT_NULL = "/app/commands/querys/SetRefClientNull.txt";
	
	private static final String DELETE_CLIENT = "/app/commands/querys/DeleteClient.txt";

	public RemoveClientCommand() {
		super();
	}

	@Override
	public void execute() {
		Integer id;
		Connection con = null;
		try {
			System.out.print("Client ID to Remove: ");
			id = s.nextInt();
			
			con = connection.getConnection();
			con.setAutoCommit(false);
			
			pstmt = con.prepareStatement(handler.getQuery(DELETE_ORDER));
			pstmt.setInt(1, id);
			pstmt.execute();
			
			pstmt = con.prepareStatement(handler.getQuery(DELETE_MANAGE_MANAGER_WALLET));
			pstmt.setInt(1, id);
			pstmt.execute();
			
			pstmt = con.prepareStatement(handler.getQuery(DELETE_RESP_MANAGER_WALLET));
			pstmt.setInt(1, id);
			pstmt.execute();
			
			pstmt = con.prepareStatement(handler.getQuery(DELETE_SHARE_WALLET_REGISTER));
			pstmt.setInt(1, id);
			pstmt.execute();
			
			pstmt = con.prepareStatement(handler.getQuery(DELETE_SHARE_WALLET));
			pstmt.setInt(1, id);
			pstmt.execute();
			
			pstmt = con.prepareStatement(handler.getQuery(DELETE_WALLET_REGISTER));
			pstmt.setInt(1, id);
			pstmt.execute();
			
			pstmt = con.prepareStatement(handler.getQuery(DELETE_WALLET));
			pstmt.setInt(1, id);
			pstmt.execute();
			
			pstmt = con.prepareStatement(handler.getQuery(SET_REF_CLIENT_NULL));
			pstmt.setInt(1, id);
			pstmt.execute();
			
			pstmt = con.prepareStatement(handler.getQuery(DELETE_CLIENT));
			pstmt.setInt(1, id);
			pstmt.execute();
			
			con.commit();
			
			System.out.println("Client succesfully removed");
		} catch (SQLException e) {
			try {
				if(con != null){
					con.rollback();
					System.out.println("Error Ocurred, transaction has been rolledBack");
				}
			} catch (SQLException e1) 
			{
				e1.printStackTrace();
			}
		}
		finally{
			try {
				if(con != null)
					con.setAutoCommit(true);
				
				this.closeAll();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
