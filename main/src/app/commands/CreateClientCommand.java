package app.commands;

import java.sql.SQLException;
import java.sql.Types;

import app.commands.factory.Command;
import app.commands.factory.CommandInterface;
import app.sql.utils.SQLUtils;
import app.sql.utils.TypeOfStatementAceptedData;

public class CreateClientCommand extends Command implements CommandInterface{
	
	private static final String PATH = "/app/commands/querys/CreateClientQuery.txt";
	
	public CreateClientCommand() {
		super();
	}

	@Override
	public void execute() {
		Integer id, nif, limit;
		String name, contractType, clientType, reference;

		try {
			System.out.print("New Client ID: ");
			id = s.nextInt();
			System.out.print("NIF: ");
			nif = s.nextInt(); s.nextLine();
			System.out.print("Name: ");
			name = s.nextLine();
			System.out.print("Contract Type: ");
			contractType = s.nextLine();
			System.out.print("Client Type: ");
			clientType = s.nextLine();
			System.out.print("Limit: ");
			limit = s.nextInt(); s.nextLine();
			System.out.print("Referenced by: ");
			reference = s.next();
			
			pstmt = connection.getConnection().prepareStatement(handler.getQuery(PATH));
			pstmt.setInt(1, id);
			pstmt.setInt(2, nif);
			pstmt.setString(3, name);
			pstmt.setString(4, contractType.toUpperCase());
			pstmt.setString(5, clientType.toUpperCase());
			pstmt.setInt(6, limit);
			SQLUtils.setInPreparedStatement(pstmt, Types.INTEGER, TypeOfStatementAceptedData.Int, 7, reference);
			pstmt.execute();
			
			System.out.printf("%s successfully introduced", name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				this.closeAll();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
