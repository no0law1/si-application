package app.commands;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import app.commands.factory.Command;
import app.commands.factory.CommandInterface;
import app.sql.utils.SQLUtils;

public class WalletToRegisterCommand extends Command implements CommandInterface{
	
	private static final String GET_WALLET_QUERY_CLOSEVAL = "/app/commands/querys/WalletToRegisterQuery1.txt";

	private static final String INSERT_WALLET_REGISTER=  "/app/commands/querys/WalletToRegisterQuery2.txt";
	
	private static final String GET_ALL_SHARES_WALLET = "/app/commands/querys/WalletToRegisterQuery3.txt";
	
	private static final String INSERT_SHARE_REGISTER = "/app/commands/querys/WalletToRegisterQuery4.txt";
	
	public WalletToRegisterCommand() {
		super();
	}
	
	@Override
	public void execute() {
		Integer codWallet, registerID;
		Timestamp datetime;
		Float valClose;
		ResultSet rs = null;
		Connection con = null;
		try {
			System.out.print("Wallet ID: ");
			codWallet = s.nextInt(); s.nextLine();
			
			pstmt = connection.getConnection().prepareStatement(handler.getQuery(GET_WALLET_QUERY_CLOSEVAL));
			pstmt.setInt(1, codWallet);
			rs = pstmt.executeQuery();
			if(!rs.next()){
				System.out.println("No Wallet");
				return;
			}
			valClose = rs.getFloat(1);
			this.closeAll();
			rs.close();
			
			System.out.print("Register ID: ");
			registerID = s.nextInt(); s.nextLine();

			System.out.print("Date: ");
			datetime = SQLUtils.stringToDate(s.nextLine());
			
			con = connection.getConnection();
			con.setAutoCommit(false);
			
			pstmt = con.prepareStatement(handler.getQuery(INSERT_WALLET_REGISTER));
			pstmt.setInt(1, registerID);
			pstmt.setInt(2, codWallet);
			pstmt.setTimestamp(3, datetime);
			pstmt.setFloat(4, valClose);
			
			pstmt.execute();
			
			pstmt = con.prepareStatement(handler.getQuery(GET_ALL_SHARES_WALLET));
			pstmt.setInt(1, codWallet);
			rs = pstmt.executeQuery();
			
			pstmt = con.prepareStatement(handler.getQuery(INSERT_SHARE_REGISTER));
			pstmt.setInt(1, registerID);
			pstmt.setInt(2, codWallet);
			pstmt.setTimestamp(5, datetime);
			while(rs.next()) {
				pstmt.setInt(3, rs.getInt(1));
				pstmt.setInt(4, rs.getInt(2));
				pstmt.setFloat(6, rs.getFloat(3));
				pstmt.setInt(7, rs.getInt(4));
				pstmt.execute();
			}
			con.commit();
			
			System.out.printf("Wallet %d successfully introduced to register", codWallet);
		} catch (SQLException e) {
				try {
					if(con!=null){
						con.rollback();
						System.out.println("Error Ocurred, transaction has been rolledBack");
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			e.printStackTrace();
		}
		finally{
			try {
				if(rs!=null)
					rs.close();
				if(con != null)
					con.setAutoCommit(true);
				
				this.closeAll();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}	
}
