package app.commands.factory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import app.querys.QueryHandler;
import app.sql.Connection;

public abstract class Command implements CommandInterface{
	
	protected final static Scanner s = new Scanner(System.in);
	
	protected Connection connection;
	
	protected QueryHandler handler;
	
	protected PreparedStatement pstmt;
	
	public Command() {
		connection = new Connection();
		handler = new QueryHandler();
		pstmt = null;
	}
	
	protected void closeAll() throws SQLException{
		if(pstmt != null)
			pstmt.close();
		connection.closeConnection();
	}
	
}
