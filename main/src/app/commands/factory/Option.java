package app.commands.factory;

public enum Option{
	Unknown,
	ClientData,
	CreateClient,
	CreateWallet,
	WalletToRegister,
	CreateOrder,
	RemoveClient,
	ListShares,
	ShowNASDAQShares,
	Exit
}
