package app.commands;

import java.sql.*;

import app.commands.factory.Command;
import app.commands.factory.CommandInterface;
import app.sql.utils.SQLUtils;

public class ClientDataCommand extends Command implements CommandInterface{
	
	private static final String PATH = "/app/commands/querys/ClientDataQuery.txt";
	
	public ClientDataCommand() {
		super();
	}
	
	@Override
	public void execute() {
		Integer nif;
		ResultSet rs = null;		
		try {
			System.out.print("Client NIF: ");
			nif = s.nextInt();
			
			pstmt = connection.getConnection().prepareStatement(handler.getQuery(PATH));
			pstmt.setInt(1, nif);
			rs = pstmt.executeQuery();
			System.out.println();
			SQLUtils.printResultSet(rs);
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			try {
				if(rs!=null)
					rs.close();
				this.closeAll();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
