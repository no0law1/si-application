package app.commands;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import app.commands.factory.Command;
import app.commands.factory.CommandInterface;
import app.sql.utils.SQLUtils;
import app.sql.utils.TypeOfStatementAceptedData;

public class CreateOrderCommand extends Command implements CommandInterface{
	
	private static final String INSERT_ORDER = "/app/commands/querys/CreateOrderQuery1.txt";
	
	private static final String GET_SHARE_WALLET = "/app/commands/querys/CreateOrderQuery2.txt";
	
	private static final String CREATE_SHARE_WALLET = "/app/commands/querys/CreateOrderQuery3.txt";
	
	private static final String BUY_QUERY = "/app/commands/querys/CreateOrderQuery4.txt";
	
	private static final String SELL_QUERY = "/app/commands/querys/CreateOrderQuery5.txt";
	
	public CreateOrderCommand() {
		super();
	}

	@Override
	public void execute() {
		Integer id, quantity, shareId, marketId, codWallet;
		String orderType, operationType, clientId, managerId;
		Timestamp orderDate, validityDate;
		Float value, orderCost;
		ResultSet rs = null;
		Connection con = null;
		
		try{
			System.out.print("Wallet ID: ");
			codWallet = s.nextInt();
			
			System.out.print("Share ID: ");
			shareId = s.nextInt();
			
			System.out.print("Market ID: ");
			marketId = s.nextInt(); s.nextLine();
			
			System.out.print("Order Type: ");
			orderType = s.nextLine().toUpperCase();
			
			System.out.print("Order ID: ");
			id = s.nextInt();
			
			System.out.print("Value: ");
			value = s.nextFloat();
			
			System.out.print("Quantity: ");
			quantity = s.nextInt(); s.nextLine();
			
			System.out.print("Operation Type: ");
			operationType = s.nextLine();
			
			System.out.print("Order Date: ");
			orderDate = SQLUtils.stringToDate(s.nextLine());
		
			System.out.print("Order Validity Date: ");
			validityDate = SQLUtils.stringToDate(s.nextLine());
			
			System.out.print("Cost: ");
			orderCost = s.nextFloat(); s.nextLine();
			
			System.out.print("Client ID: ");
			clientId = s.nextLine(); 
			
			System.out.print("Manager ID: ");
			managerId = s.nextLine();
			
			con = connection.getConnection();
			con.setAutoCommit(false);
			
			pstmt = con.prepareStatement(handler.getQuery(INSERT_ORDER));
			pstmt.setInt(1, id);
			pstmt.setFloat(2, value);
			pstmt.setInt(3, quantity);
			pstmt.setString(4, orderType.toUpperCase());
			pstmt.setString(5, operationType.toUpperCase());
			pstmt.setTimestamp(6, orderDate);
			pstmt.setTimestamp(7, validityDate);
			pstmt.setFloat(8, orderCost);
			pstmt.setString(9, "PENDENT");
			pstmt.setInt(10, shareId);
			pstmt.setInt(11, marketId);
			pstmt.setNull(12, Types.INTEGER);
			SQLUtils.setInPreparedStatement(pstmt, Types.INTEGER, TypeOfStatementAceptedData.Int, 13, clientId);
			SQLUtils.setInPreparedStatement(pstmt, Types.INTEGER, TypeOfStatementAceptedData.Int, 14, managerId);

			pstmt.execute();
			
			System.out.printf("Pending Order successfully introduced.\nExecuting Order...\n");
			
			pstmt = con.prepareStatement(handler.getQuery(GET_SHARE_WALLET));
			pstmt.setInt(1, codWallet);
			pstmt.setInt(2, shareId);
			pstmt.setInt(3, marketId);
			rs = pstmt.executeQuery();
			
			if(!rs.next()){
				if(orderType.equals("SELL")){
					throw new SQLException();
				}
				else{
					pstmt = con.prepareStatement(handler.getQuery(CREATE_SHARE_WALLET));
					pstmt.setInt(1, codWallet);
					pstmt.setInt(2, shareId);
					pstmt.setInt(3, marketId);
					pstmt.setInt(4, quantity);
					pstmt.execute();
				}
			}
			else{
				pstmt = (orderType.equals("SELL")) ?
					con.prepareStatement(handler.getQuery(SELL_QUERY)) :
					con.prepareStatement(handler.getQuery(BUY_QUERY));
					
				pstmt.setInt(1, quantity);
				pstmt.setInt(2, codWallet);
				pstmt.setInt(3, marketId);
				pstmt.setInt(4, shareId);
				pstmt.execute();
			}
			
			pstmt = con.prepareStatement(handler.getQuery(INSERT_ORDER));
			pstmt.setInt(1, id+1);
			pstmt.setFloat(2, value);
			pstmt.setInt(3, quantity);
			pstmt.setString(4, orderType.toUpperCase());
			pstmt.setString(5, operationType.toUpperCase()); 

			pstmt.setTimestamp(6,orderDate);
			pstmt.setTimestamp(7,validityDate);
			pstmt.setFloat(8,orderCost);
			pstmt.setString(9, "SATISFIED");
			pstmt.setInt(10,shareId);
			pstmt.setInt(11,marketId);
			pstmt.setInt(12, id);
			SQLUtils.setInPreparedStatement(pstmt, Types.INTEGER, TypeOfStatementAceptedData.Int, 13, clientId);
			SQLUtils.setInPreparedStatement(pstmt, Types.INTEGER, TypeOfStatementAceptedData.Int, 14, managerId);
			
			pstmt.execute();
			con.commit();
			System.out.println("Order successfully executed");
		}
		catch (SQLException e) {
			try{
				if(con!=null){
					con.rollback();
					System.out.println("Error Ocurred, transaction has been rolledBack");
				}
			}
			catch(SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		finally{
			try {
				if(con != null)
					con.setAutoCommit(true);
				if(rs!=null)
					rs.close();
				
				this.closeAll();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
}