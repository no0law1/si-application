package app;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Scanner;

import app.commands.ClientDataCommand;
import app.commands.CreateClientCommand;
import app.commands.CreateOrderCommand;
import app.commands.CreateWalletCommand;
import app.commands.ListSharesCommand;
import app.commands.RemoveClientCommand;
import app.commands.ShowNASDAQSharesCommand;
import app.commands.WalletToRegisterCommand;
import app.commands.factory.Command;
import app.commands.factory.Option;

public class App {
	
	private static App app = null;
	private HashMap<Option, Command> commands;
	
	private App() {
		commands = createCommands();
	}
	
	/**
	 * Hash Map with the commands of the application.
	 * @return
	 */
	public HashMap<Option, Command> createCommands(){
		HashMap<Option, Command> commands = new HashMap<Option, Command>();
		commands.put(Option.ClientData, new ClientDataCommand());
		commands.put(Option.CreateClient, new CreateClientCommand());
		commands.put(Option.CreateWallet, new CreateWalletCommand());
		commands.put(Option.WalletToRegister, new WalletToRegisterCommand());
		commands.put(Option.CreateOrder, new CreateOrderCommand());
		commands.put(Option.RemoveClient, new RemoveClientCommand());
		commands.put(Option.ListShares, new ListSharesCommand());
		commands.put(Option.ShowNASDAQShares, new ShowNASDAQSharesCommand());
		commands.put(Option.Exit, null);
		
		return commands;
	}
	
	public static App bootApp(){
		if(app==null){
			app = new App();
		}
		return app;
	}
	
	@SuppressWarnings("resource")
	private Option displayMenu() {
		Option option=Option.Unknown;
	  	try{
			System.out.println("Commands");
			System.out.println();
			System.out.println("1. Client Data");
			System.out.println("2. Create Client");
			System.out.println("3. Create Wallet");
			System.out.println("4. Wallet to Register");
			System.out.println("5. Create Order");
			System.out.println("6. Remove Client");
			System.out.println("7. List Shares");
			System.out.println("8. Show NASDAQ Shares");
			System.out.println("9. Exit");
			System.out.print(">");
			int result = new Scanner(System.in).nextInt();
			option = Option.values()[result];			
		}
		catch(RuntimeException ex){}
	  	
		return option;
	}
	
	private void run() throws SQLException {
		Option userInput = Option.Unknown;
		do{
		  	userInput = displayMenu();
			try{		
			  	commands.get(userInput).execute();
			  	System.in.read();
			}
			catch(NullPointerException | IOException ex){}
		}while(userInput!=Option.Exit);
	}

	public static void main(String[] args) {
		try {
			App.bootApp().run();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Connection not successfull");
		}
	}
	
}
